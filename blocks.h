//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/*{"[",  "",                                      0,    0},*/
	{"\x01^c#ffff00^", "~/.dwm/status-torrent",    60,    0},
	{"\x02^c#9ece6a^", "~/.dwm/status-updates",   300,    0},
	{"\x03^c#ff6c6b^", "~/.dwm/status-cpu",         3,    0},
	{"\x04^c#ff6c6b^", "~/.dwm/status-temp",       10,    0},
	{"\x05^c#4dd0e1^", "~/.dwm/status-mem",        60,    0},
	{"\x06^c#ff9e64^", "~/.dwm/status-date",       10,    0},
	/*{"]",  "",                                      0,    0},*/
};

//sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char delim[] = "^d^";
static unsigned int delimLen = 5;
